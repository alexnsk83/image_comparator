﻿using System;
using System.Collections.Generic;
using System.IO;


namespace ImageCopmarator
{
    public class ConfigLoader
    {
        string configFile = "\\config.ini";
        string currentPath = Directory.GetCurrentDirectory();
        const int SIZE = 1024;
        
        public ConfigLoader()
        {
            if (!File.Exists(currentPath + configFile))
            {
                Console.WriteLine("Файл конфигурации не найден");
                Console.ReadKey();
                Environment.Exit(0);
            }
        }

        public string GetCurrentPath()
        {
            return currentPath;
        }

        public Dictionary<String, String> GetConfig()
        {
            string[] file = File.ReadAllLines(currentPath + configFile);
            Dictionary<String, String> config = new Dictionary<string, string>();

            foreach(string line in file)
            {
                string[] array = line.Split('=');
                if (array.Length < 2)
                {
                    Console.WriteLine("Ошибка файла конфигурации. Не удалось сформировать пару ключ-значение");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
                config.Add(array[0], array[1]);
            }
            
            return config;
        }
    }
}
