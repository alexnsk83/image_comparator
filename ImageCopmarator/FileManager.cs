﻿using System;
using System.IO;

namespace ImageCopmarator
{
    public class FileManager
    {
        public bool IsFileExists(string[] files)
        {
            foreach(string file in files)
            {
                
                if(!File.Exists(file))
                {
                    Console.WriteLine("Файл " + file + " не найден");
                    Console.ReadKey();
                    return false;
                }
            }
            return true;
        }
    }
}
