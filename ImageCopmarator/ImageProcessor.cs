﻿using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.Windows;



namespace ImageCopmarator
{
    public static class ImageProcessor
    {
        public static Bitmap GetBitmap(string file)
        {
            BitmapSource bitmapSource = TiffDecode(file);
            Bitmap bmp = new Bitmap(bitmapSource.PixelWidth, bitmapSource.PixelHeight, PixelFormat.Format1bppIndexed);
            BitmapData data = bmp.LockBits(new Rectangle(System.Drawing.Point.Empty, bmp.Size), ImageLockMode.WriteOnly, PixelFormat.Format1bppIndexed);
            bitmapSource.CopyPixels(Int32Rect.Empty, data.Scan0, data.Height * data.Stride * 2, data.Stride);
            bmp.UnlockBits(data);

            return bmp;
        }


        public static bool CompareDimensions(Bitmap img1, Bitmap img2)
        {
            if(img1.Size == img2.Size)
            {
                return true;
            }
            return false;
        }


        public static void Processing(byte[] rgbValues1, byte[] rgbValues2, byte[] resultRgbValues, int thread, int threadTotal)
        {
            int start = rgbValues1.Length / threadTotal * (thread - 1);
            int end = rgbValues1.Length / threadTotal * thread;
            for (int i = start; i < end; i += 3)
            {
                if (rgbValues1[i] > rgbValues2[i])
                {
                    resultRgbValues[i + 1] = resultRgbValues[i + 2] = 0;
                    resultRgbValues[i] = 255;
                }
                if (rgbValues1[i] < rgbValues2[i])
                {
                    resultRgbValues[i] = resultRgbValues[i + 1] = 0;
                    resultRgbValues[i + 2] = 255;
                }
            }
        }


        static BitmapSource TiffDecode(string file)
        {
            Stream imageStreamSource = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read);
            TiffBitmapDecoder decoder = new TiffBitmapDecoder(imageStreamSource, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
            BitmapSource bitmapSource = decoder.Frames[0];

            return bitmapSource;
        }
    }
}
