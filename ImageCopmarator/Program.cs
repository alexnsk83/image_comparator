﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Drawing;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace ImageCopmarator
{
    class Program
    {
        static void Main(string[] args)
        {
            string file1;
            string file2;
            string path;

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            ConfigLoader ConfigLoader = new ConfigLoader();
            FileManager fileManager = new FileManager();
            Dictionary<String, String> config = ConfigLoader.GetConfig();

            path = ConfigLoader.GetCurrentPath() + "\\";
            file1 = path + config["image1"];
            file2 = path + config["image2"];
            string[] files = { file1, file2 };

            if (fileManager.IsFileExists(files))
            {
                Bitmap image1 = ImageProcessor.GetBitmap(file1);
                image1 = image1.Clone(new Rectangle(0, 0, image1.Width, image1.Height), PixelFormat.Format24bppRgb);
                Bitmap image2 = ImageProcessor.GetBitmap(file2);
                image2 = image2.Clone(new Rectangle(0, 0, image1.Width, image1.Height), PixelFormat.Format24bppRgb);
                Bitmap resultImage = image1.Clone(new Rectangle(0, 0, image1.Width, image1.Height), PixelFormat.Format24bppRgb);

                Rectangle rect = new Rectangle(0, 0, image1.Width, image1.Height);

                BitmapData data1 = image1.LockBits(rect, ImageLockMode.ReadOnly, image1.PixelFormat);
                BitmapData data2 = image2.LockBits(rect, ImageLockMode.ReadOnly, image2.PixelFormat);
                BitmapData resultData = resultImage.LockBits(rect, ImageLockMode.ReadWrite, resultImage.PixelFormat);

                IntPtr ptr1 = data1.Scan0;
                IntPtr ptr2 = data2.Scan0;
                IntPtr resultPtr = resultData.Scan0;

                int bytesCount = Math.Abs(data1.Stride) * image1.Height;

                byte[] rgbValues1 = new byte[bytesCount];
                byte[] rgbValues2 = new byte[bytesCount];
                byte[] resultRgbValues = new byte[bytesCount];

                Marshal.Copy(ptr1, rgbValues1, 0, bytesCount);
                Marshal.Copy(ptr2, rgbValues2, 0, bytesCount);
                Marshal.Copy(resultPtr, resultRgbValues, 0, bytesCount);

                int threadCount = 1;
                Thread[] threadsArray = new Thread[threadCount];

                for (int i = 0; i < threadCount; i++)
                {
                    int localVar = i;
                    threadsArray[i] = new Thread(() => ImageProcessor.Processing(rgbValues1, rgbValues2, resultRgbValues, localVar+1, threadCount));
                    threadsArray[i].Start();
                }
                for (int i = 0; i < threadCount; i++)
                {
                    threadsArray[i].Join();
                }

                Marshal.Copy(resultRgbValues, 0, resultPtr, bytesCount);
                resultImage = resultImage.Clone(new Rectangle(0, 0, image1.Width, image1.Height), PixelFormat.Format8bppIndexed);
                resultImage.Save("result.tif", ImageFormat.Tiff);
                Console.WriteLine("Обработка выполнена за: " + stopwatch.Elapsed);
                Console.ReadKey();
            }
        }
    }
}
